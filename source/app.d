// Copyright (C) 2019 Antonio Corbi
// based on an idea from Daniel Shiffman (shiffman.net)
// "Maurer's Rose as in https://youtu.be/4uU9lZ-HSqA"

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

module app;

import std.stdio;

import gio.Application : GApplication = Application;
import gtk.Application;

import appwindow;

class GtkDApp : Application
{
public:

  this()
  {
    ApplicationFlags flags = ApplicationFlags.FLAGS_NONE;
    super("org.example.GtkDApp", flags);
    this.addOnActivate(&onAppActivate);
    this.window = null;
  }

private:
  AppWindow window;

  void onAppActivate(GApplication app)
  {
    this.window = new AppWindow(this);
    this.window.present();
  }
}
