// Copyright (C) 2019 Antonio Corbi
// based on an idea from Daniel Shiffman (shiffman.net)
// "Maurer's Rose as in https://youtu.be/4uU9lZ-HSqA"

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

module appwindow;

import std.path, std.stdio;

import gtk.ApplicationWindow,
  gtk.Builder,
  gtk.HeaderBar,
  gtk.Label,
  gtk.ScrolledWindow,
  gtk.Box,
  gtk.Main,
  gtk.Scale,
  gtk.Button,
  gtk.Widget,
  gtk.DrawingArea;

import gdk.Cairo, cairo.Context;

import constants;

// In case you are in GNU/Linux and have sed...
immutable string pkgdatadir = DATADIR;

// So you don't have sed at hand....assume the data folder is in the
// current directory, i.e. where you issue the 'dub run' command.'
//immutable string pkgdatadir = "./data/";

immutable uint N = 6;
immutable double D = 71.0;
immutable double SCALE = 255.0;

struct Point {
  double x;
  double y;
}

/**
 * This is the Application Window.
 * Everything starts here.
 */
class AppWindow: gtk.ApplicationWindow.ApplicationWindow
{
public:

  /// Constructor for the Window
  this(gtk.Application.Application application)
  {
    super(application);
    theApp = application;
    builder = new Builder();

    if(!builder.addFromFile(buildPath(pkgdatadir,"ui/ui.glade")))
      {
        writeln("Window ui-file cannot be found");
        return;
      }

    HeaderBar headerBar = cast(HeaderBar) builder.getObject("headerBar");
    this.setTitlebar(headerBar);

    // Window contents from glade, we pull them out the window created
    // in glade and push them into 'this' window. Gtk used to have a
    // function called 'reparent' now deprecated.
    Box windowContent = cast(Box) builder.getObject("wcontents");
    windowContent.unparent();
    this.add(windowContent);

    // Quit button
    auto qbutton = cast(Button) builder.getObject("quitbutton");
    qbutton.addOnClicked ((b) => theApp.quit());

    // nScale
    auto nscale = cast(Scale) builder.getObject("nvalue");
    nscale.setValue(cast(double) N);
    nscale.addOnValueChanged ((s) => n_value_changed(cast(Scale) s));

    // dScale
    auto dscale = cast(Scale) builder.getObject("dvalue");
    dscale.setValue(cast(double) D);
    dscale.addOnValueChanged ((s) => d_value_changed(cast(Scale) s));

    // da : Drawing Area
    da = cast(DrawingArea) builder.getObject("darea");
    da.addOnDraw (&redrawPage);

    // Compute first rose:
    n = N;
    d = D;
    maurer_rose;
  }

  /// Maurer Rose computation function
  void maurer_rose () {
    import std.math : sin, cos, PI;

    double to_radians (double dg) {
      return (dg * (PI / 180.0));
    }

    for (uint i = 0; i < 361; i++) {

      double k = i * d;
      double angle = to_radians(n * k);
      double r = SCALE * sin(angle);
      double x = r * cos(to_radians(k));
      double y = r * sin(to_radians(k));

      pts[i].x = x; pts[i].y = y;

      // Petal shape points computation
      double ik = cast(double) i;
      angle = to_radians(n * ik);
      r = SCALE * sin(angle);
      x = r * cos(to_radians(ik));
      y = r * sin(to_radians(ik));

      ipts[i].x = x; ipts[i].y = y;
    }
  }

private:

  void n_value_changed (Scale s) {
    n = cast(uint) s.getValue();
    maurer_rose;
    da.queueDraw();
  }

  void d_value_changed (Scale s) {
    d = s.getValue();
    maurer_rose;
    da.queueDraw();
  }

  bool redrawPage (Scoped!Context c, Widget wda) {
    double w = cast(double) wda.getAllocatedWidth();
    double h = cast(double) wda.getAllocatedHeight();

    c.translate(w / 3.0, h / 3.0);

    c.setLineWidth(1);
    c.moveTo(pts[0].x, pts[0].y);
    c.setSourceRgb(0, 0, 0.9);
    foreach (p ; pts) {
      c.lineTo(p.x, p.y);
    }
    c.stroke();

    c.setLineWidth(3);
    c.moveTo(ipts[0].x, ipts[0].y);
    c.setSourceRgb(.9, 0, 0);
    foreach (p ; ipts) {
      c.lineTo(p.x, p.y);
    }
    c.stroke();

    return false;
  }

  // Data -----------------------------------------------------------------
  private gtk.Application.Application theApp;
  private DrawingArea da;
  private Builder builder;
  private Point[361] pts;
  private Point[361] ipts;
  private uint n;
  private double d;
}
